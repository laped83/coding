#!/bin/bash

# Description: This script will put the NAS device into s3 sleep when no LAN activity has been detected within 1 hour. You can adjust the NO_ACTIVITY_LIMIT_SECS and the crontab to fit your needs.
# Installation instructions (SSH required)
# 1) Add sleeper.sh to persistent location, like a USB flash drive (not mechanical drives to keep spindown working).
# 2) Remember to chmod +x sleeper.sh to make the file executable.
# 3) Update LAN_NET if needed. (Default for this script is 192.168, which works if NAS is within 192.168.0.0/16 range)
# 4) Add cronjob. Following example tries to start sleeper.sh every hour. The script will exit if already running.
# Note: After a S3 sleep the startup can take up to 30 minutes before the script is started again (via cronjob) and 1 hour (NO_ACTIVITY_LIMIT_SECS) of no activity before it enters sleep mode again.
# Note: That any jobs like remote backup/replication may be affected.

#[~] # cat /etc/config/crontab
# m h dom m dow cmd
#15,45 * * * * /share/external/DEV3303_0/sleeper.sh
#0 2 * * * /sbin/qfstrim
#10 15 * * * /usr/bin/power_clean -c 2>/dev/null

# Defines
NO_ACTIVITY_LIMIT_SECS="3600"
ACTIVITY_CHECK_STEPS_SECS="300"
LAN_NET="192.168"
MY_PID_FILE=/tmp/sleeper.sh.pid

# Initial
LAST_ACTIVITY=$(date +%s)

# Exit if script is already running.
if [ -s "$MY_PID_FILE" ]; then
    exit
fi

# Set trap to ensure pid file is removed on exit
trap 'rm -f -- "$MY_PID_FILE"' EXIT

# Create pid file
echo $$ > "$MY_PID_FILE"

# Main loop
while true; do
    # check for LAN activity using netstat. Double LAN_NET Regex ensures only looking at LAN traffic
    LAN_ACTIVITY=$(netstat -tnu | grep -c ".*$LAN_NET.*$LAN_NET.*")
    TIME_NOW=$(date +%s)
    if [ "$LAN_ACTIVITY" -ne 0 ]; then
        # Activity reset LAST_ACTIVITY to current time
        LAST_ACTIVITY="$TIME_NOW"
    else
        TIME_MAX_TIMESTAMP=$((TIME_NOW - NO_ACTIVITY_LIMIT_SECS))
        # Put to S3 sleep if LAST_ACTIVITY is more than NO_ACTIVITY_LIMIT_SECS
        if [ "$LAST_ACTIVITY" -lt "$TIME_MAX_TIMESTAMP" ]; then
            # Check disk volume status. Dont force sleep on diskscan etc.
            DISK_STATUS=$(getsysinfo vol_status 1)
            if [[ "$DISK_STATUS" == "Ready" ]]; then
                /etc/init.d/pw_sleep.sh &
                break
            fi
        fi
    fi
    sleep "$ACTIVITY_CHECK_STEPS_SECS"
done